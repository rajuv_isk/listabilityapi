<?php
if (realpath(__FILE__) == realpath($_SERVER['SCRIPT_FILENAME'])) {
    exit('Please don\'t access this file directly.');
}
add_action('wpcf7_before_send_mail', 'cf7d_before_send_email_data');
function cf7d_before_send_email_data($contact_form)
{
    global $wpdb;
	$emaildata_ser = serialize($_POST);
	$date = date("Y-m-d H:i:s");
	$insert = array( 'cf7_id'      		   => $_POST['_wpcf7'],
					 'ip_address'  		   => $_SERVER['REMOTE_ADDR'],
					 'browser_information' => $_SERVER['HTTP_USER_AGENT'],
					 'form_title'         =>  $contact_form->title,
					 'form_data' 		   => $emaildata_ser,
					 'd_date'			   => $date
					);
	$wpdb->insert(OEPL_EMAIL_FIELDS, $insert);
}
?>