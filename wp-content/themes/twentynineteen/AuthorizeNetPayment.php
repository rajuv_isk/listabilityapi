<?php
require 'sdk-php-master/autoload.php';
use net\authorize\api\contract\v1 as AnetAPI;
use net\authorize\api\controller as AnetController;

class AuthorizeNetPayment
{

    private $APILoginId;
    private $APIKey;
    private $refId;
    private $merchantAuthentication;
    public $responseText;


    public function __construct()
    {
        /*test mode*/
        if(get_field('payment_mode','options')=='test'){
            $test_detailsarr = get_field('test_details','options');
            if(!empty($test_detailsarr)){
                $this->APILoginId = $test_detailsarr['apiloginid'];
                $this->APIKey = $test_detailsarr['apikey'];
            }else{
                /*allowed test details*/
                $this->APILoginId = '573Vp7b2Dgc';
                $this->APIKey = '85pGj2Ee39R9UQcg';
            }
            
        }elseif(get_field('payment_mode','options')=='live'){
            $live_detailsarr =get_field('live_details','options');
            if(!empty($live_detailsarr)){
                $this->APILoginId = $live_detailsarr['liveapiloginid'];
                $this->APIKey = $live_detailsarr['liveapikey'];
            }else{
                /*allowed test details*/
                $this->APILoginId = '573Vp7b2Dgc';
                $this->APIKey = '85pGj2Ee39R9UQcg';
            }
        }else{
            /*allowed test details*/
            $this->APILoginId = '573Vp7b2Dgc';
            $this->APIKey = '85pGj2Ee39R9UQcg';
        }

        $this->refId = 'ref' . time();
        
        $this->merchantAuthentication = $this->setMerchantAuthentication();
        $this->responseText = array("1"=>"Approved", "2"=>"Declined", "3"=>"Error", "4"=>"Held for Review");
    }

    public function setMerchantAuthentication()
    {
        $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
        $merchantAuthentication->setName($this->APILoginId);
        $merchantAuthentication->setTransactionKey($this->APIKey);  
        
        return $merchantAuthentication;
    }
    
    public function setCreditCard($cardDetails)
    {
        $creditCard = new AnetAPI\CreditCardType();
        $creditCard->setCardNumber($cardDetails["card-number"]);
        $creditCard->setExpirationDate( $cardDetails["year"] . "-" . $cardDetails["month"]);
        $paymentType = new AnetAPI\PaymentType();
        $paymentType->setCreditCard($creditCard);
        
        return $paymentType;
    }
    
    public function setTransactionRequestType($paymentType, $amount)
    {
        $transactionRequestType = new AnetAPI\TransactionRequestType();
        $transactionRequestType->setTransactionType("authCaptureTransaction");
        $transactionRequestType->setAmount($amount);
        $transactionRequestType->setPayment($paymentType);
        
        return $transactionRequestType;
    }

    public function chargeCreditCard($cardDetails)
    {
        $paymentType = $this->setCreditCard($cardDetails);
        $transactionRequestType = $this->setTransactionRequestType($paymentType, $_POST["amount"]);
        
        $request = new AnetAPI\CreateTransactionRequest();
        $request->setMerchantAuthentication($this->merchantAuthentication);
        $request->setRefId( $this->refId);
        $request->setTransactionRequest($transactionRequestType);
        $controller = new AnetController\CreateTransactionController($request);
        $response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::SANDBOX);
        
        return $response;
    }
}
