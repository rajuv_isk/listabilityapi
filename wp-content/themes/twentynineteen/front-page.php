<?php 
/*
* Template Name: Home
*/
get_header();

	//phpinfo();
	 $api_key = get_field('api_key','option');
	/*echo '<br>'.$datazapp_admin_email = get_field('datazapp_admin_email','option');
	echo  '<br>'.$datazapp_password = get_field('datazapp_password','option');*/
	$message = '';
	
	/*new code */
	$csv_mimetypes = array('text/csv', 'text/plain', 'application/csv', 'text/comma-separated-values', 'application/excel', 'application/vnd.ms-excel', 'application/vnd.msexcel', 'text/anytext', 'application/octet-stream', 'application/txt');
	$target_dir = wp_upload_dir()['baseurl'];
	$target_file = $target_dir .'/'. basename($_FILES["csvfile"]["name"]);
	$uploadOk = 1;
	$imageFileType =  $_FILES["csvfile"]["type"];
	/*print_r(get_post_meta(37,'uploaded_file'));
	echo '<pre>';print_r(json_decode(get_post_meta(44,'api_response',true)));
	print_r($_FILES['csvfile']);
	print_r($_POST);die;*/

	if(!empty($_POST) && $_FILES['csvfile']['name'] !='') {
		/*echo '<pre>';print_r($_POST);
		print_r($_FILES['csvfile']);*/

		if (!in_array($imageFileType, $csv_mimetypes)) {
		    $message="Sorry, only CSV file is allowed.";
		    $uploadOk = 0;
		}else{
			$uploadOk = 1;
		}
		/*Check file size*/
		if ($_FILES["csvfile"]["size"] > 500000) {
		    $message="Sorry, your file is too large.";
		    $uploadOk = 0;
		}

		/*Check if $uploadOk is set to 0 by an error*/
		if ($uploadOk == 0) {
		   $message="Sorry, your file was not uploaded.";

		/*if everything is ok, try to upload file*/
		} else {
	
		    $uploadme = wp_upload_dir();
		    if (!function_exists('wp_handle_upload')) {
		        require_once( ABSPATH . 'wp-admin/includes/file.php' );
		    }
		    $upload_overrides = array('test_form' => false);
		    $movefile  = wp_handle_upload($_FILES['csvfile'], $upload_overrides);

			/*print_r($movefile);
		    echo "The file ". $movefile["url"]. " has been uploaded.";*/
		   	
		   	if (is_array($movefile) && !isset($movefile['error'])) {
			        //echo "The file ".$movefile["url"]. " has been uploaded and ready to read";
			        
		 			$user_id = get_current_user_id();
		   			$postarr = array(
		   					'post_author'           => $user_id,
					        'post_content'          => '',
					        'post_content_filtered' => '',
					        'post_title'            => end(explode('/',$movefile["url"])),
					        'post_excerpt'          => '',
					        'post_status'           => 'publish',
					        'post_type'             => 'listorders',
					        'post_parent'           => 0,
					        'menu_order'            => 0,
		   			);
			        $post_id =wp_insert_post($postarr);

			        $contarr = array();
			        $dataemlarray=array();
			        $datapharray=array();

			        $finalemljsondata=array('ApiKey'=>$api_key,'AppendModule'=>'EmailAppend','AppendType'=> 1,'Data'=>'');
			        $finalphjsondata=array('ApiKey'=>$api_key,'AppendModule'=>'PhoneAppendAPI','AppendType'=> 2,'DncFlag'=>true,'Data'=>'');
			        $count=1;
			        $rawCSV = fopen($movefile["url"], "r");

					while(!feof($rawCSV)) {
						$linearr = fgetcsv($rawCSV, 1000, ',', '"');
						if($count!=1 ){
							array_push($contarr, $linearr);	
						}			
						$count++;
					}
					fclose($rawCSV);
					//echo '<pre>';print_r($contarr);

					foreach ($contarr as $conar) {	
						/*code for data email array*/
						$demlarr['FirstName']=$dpharr['FirstName']=$conar[2];
						$demlarr['LastName']=$dpharr['LastName']=$conar[4];
						if($conar[6]!=''){
							$demlarr['Address']=$dpharr['Address']=$conar[6].', '.$conar[5];	
						}else{
							$demlarr['Address']=$dpharr['Address']=$conar[5];
						}						
						$demlarr['City']=$dpharr['City']=$conar[7]; 
						$demlarr['Zip']=$dpharr['Zip']=$conar[10];
						/*code for data email array end here*/

						/*code for data phone array*/
						$dpharr['AlterFirstName']=$conar[1];
						$dpharr['AlterLastName']='';
						$dpharr['AlterAddress']=$conar[16].' '.$conar[17];
						$dpharr['AlterCity']=$conar[19];
						$dpharr['AlterZip']=$conar[20];
						/*code for data email array end here*/

						array_push($dataemlarray, $demlarr);
						array_push($datapharray, $dpharr);
						
					}
					//echo '<pre>';print_r($dataemlarray);
					//echo '<pre>';print_r($datapharray); 
										
					if(!empty($dataemlarray)){
						$finalemljsondata['Data']=$dataemlarray;
					}else{
						$finalemljsondata['Data']='';
					}

					if(!empty($datapharray)){
						$finalphjsondata['Data']=$datapharray;
					}else{
						$finalphjsondata['Data']='';
					}

					/*echo '<pre>';print_r($finalemljsondata);*/
					$myemlJSON = json_encode($finalemljsondata);
					$myphJSON = json_encode($finalphjsondata);
					/*echo '<pre>';print_r($myemlJSON);
					echo '<pre>';print_r($myphJSON);*/


					/*Api call and get response*/
					$url = 'https://secureapi.datazapp.com/Appendv2'; /*API URL*/
					$ch = curl_init($url); 	/*//create a new cURL resource*/
					curl_setopt($ch, CURLOPT_POSTFIELDS, $myemlJSON); 	/*//attach encoded JSON string to the POST fields*/
					curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json')); 	/*//set the content type to application/json*/
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);  /*//return response instead of outputting*/
					$result = curl_exec($ch); 	/*//execute the POST request*/
					curl_close($ch); 	/*//close cURL resource*/
					/*echo 'hello<pre>';print_r($result);*/
					if(isset($post_id)){
						update_post_meta($post_id,'api_response',$result,'true');
						update_post_meta($post_id,'datazapp_token',json_decode($result)->ResponseDetail->Token,'true');
						update_post_meta($post_id,'file_owner_name',$_POST['FIRSTNAME'].'-'.$_POST['MIDINIT'].'-'.$_POST['LASTNAME'],'true');	
						update_post_meta($post_id,'address',$_POST['ADDRESS'],'true');
						update_post_meta($post_id,'file_owner_email',$_POST['email'],'true');
						update_post_meta($post_id,'uploaded_file',$movefile["url"],'true');
					}
					
					$token =json_decode($result)->ResponseDetail->Token;
					$OrderAmount = json_decode($result)->ResponseDetail->OrderAmount;
					$oamt = explode('$',$OrderAmount);
					
					if(!empty($post_id) &&  $token !='' && $oamt[1]!=''){
						$succes="API response has been updated in Admin Area. Click ok to proceed the payment.";
						$uri   = site_url('payment');
						$extra = 'token='.$token.'&amt='.$oamt[1].'&pid='.$post_id;
						header("Location: $uri/?$extra");
						exit;
					}else{
						echo "No token avialable.";
					}

		    } else {
		        $message="Sorry, there was an error uploading your file.";
		    }

		}
	
	}else{
		if(!empty($_POST)){
			$message="Please add the file.";	
		}
	}

?>

<div id="primary" class="content-area">
	<main id="main" class="site-main">
		

		<div class="entry-content csvform">
			<?php if($message){
				echo '<div style="color:red;">'.$message.'</div>';
			} ?>
			<?php if($succes){
				echo '<div style="color:green;">'.$succes.'</div>';
			} ?>
			
			<header class="entry-header">
				<?php get_template_part( 'template-parts/header/entry', 'header' ); ?>
			</header>

			<form method="post" action="" enctype="multipart/form-data">
				<!-- PREFIXTTL: <input type="text" name="PREFIXTTL" id=""><br />
				INDIVIDUALNAME: <input type="text" name="INDIVIDUALNAME" id=""><br />
				FIRSTNAME: <input type="text" name="FIRSTNAME" id=""><br />
				MIDINIT: <input type="text" name="MIDINIT" id=""><br />
				LASTNAME: <input type="text" name="LASTNAME" id=""><br />
				ADDRESS: <input type="text" name="ADDRESS" id=""><br />
				ADDRESS2LINE: <input type="text" name="ADDRESS2LINE" id=""><br />
				CITY: <input type="text" name="CITY" id=""><br />
				STATE: <input type="text" name="STATE" id=""><br />
				ZIP9: <input type="text" name="ZIP9" id=""><br />
				ZIP: <input type="text" name="ZIP" id=""><br />
				ZIP4: <input type="text" name="ZIP4" id=""><br />
				DPBC: <input type="text" name="DPBC" id=""><br />
				CRRT: <input type="text" name="CRRT" id=""><br />
				SITEHOUSE_V2: <input type="text" name="SITEHOUSE_V2" id=""><br />
				SITEPREDIRECTIONAL_V2: <input type="text" name="SITEPREDIRECTIONAL_V2" id=""><br />
				SITESTREETNAME_V2: <input type="text" name="SITESTREETNAME_V2" id=""><br />
				SITEADDRESS_V2: <input type="text" name="SITEADDRESS_V2" id=""><br />
				SITEUNITNUMBER_V2: <input type="text" name="SITEUNITNUMBER_V2" id=""><br />
				SITECITY_V2: <input type="text" name="SITECITY_V2" id=""><br />
				SITESTATE: <input type="text" name="SITESTATE" id=""><br />
				SITEZIPCODE: <input type="text" name="SITEZIPCODE" id=""><br />
				HOMEMARKETVALRANGE: <input type="text" name="HOMEMARKETVALRANGE" id=""><br />
				OWNERTYPEDETAIL: <input type="text" name="OWNERTYPEDETAIL" id=""><br />
				PROPTYPEDETAIL: <input type="text" name="PROPTYPEDETAIL" id=""><br />
				PURCHDATE: <input type="text" name="PURCHDATE" id=""><br />
				LOANTOVALRANGE_V2: <input type="text" name="LOANTOVALRANGE_V2" id=""><br />
				SITE_ADDR_LINES: <input type="text" name="SITE_ADDR_LINES" id=""><br /> -->

				
				<div class="firstname"><label>FIRSTNAME:</label><input type="text" name="FIRSTNAME" class="form-control" placeholder="FIRSTNAME" required></div>
				<div class="middlename"><label>MIDDLE NAME:</label><input type="text" name="MIDINIT" class="form-control" placeholder="MIDDLE NAME"></div>
				<div class="lastname"><label>LASTNAME:</label><input type="text" name="LASTNAME" class="form-control" placeholder="LASTNAME" required></div>
				<div class="fwraper"><label>ADDRESS:</label><input type="text" name="ADDRESS" class="form-control" placeholder="ADDRESS" required></div>
				<div class="fwraper"><label>EMAIL:</label><input type="email" name="email" class="form-control" placeholder="EMAIL" required></div>
				<div class="custom-csvfile"><label>CSV File To Upload:</label><input type="file" name="csvfile" id="csvfile"></div>
				<?php if(get_field('termcondition_file','options')){ ?>
				<div class="custom-chkbx"><input type="checkbox" id="subscribeNews" name="subscribe" value="newsletter" checked required>
	    			<span for="subscribeNews">Accept Terms and Conditions. Read &nbsp;<a href="<?php echo site_url('terms-and-condition');?>" target="_blank">here</a>. </span>
				<?php } ?></div>
				<div class="submit-btn"><input type="submit"/></div>
			</form>
		</div>

	</main>
</div>


<?php 
	get_footer();
?>
