<?php

/*custom code for api integration*/

/*Theme Options*/
if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title'	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Header Settings',
		'menu_title'	=> 'Header',
		'parent_slug'	=> 'theme-general-settings',
	));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Footer Settings',
		'menu_title'	=> 'Footer',
		'parent_slug'	=> 'theme-general-settings',
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Datazapp Settings',
		'menu_title'	=> 'Datazapp',
		'parent_slug'	=> 'theme-general-settings',
	));
	
}

/*Theme Options*/


/*Pagination for blog posts*/
function custompage($numpages = '', $pagerange = '', $paged='') {
	if (empty($pagerange)) {
	    $pagerange = 4;
	}


	global $paged;
	  if (empty($paged)) {
	    $paged = 1;
	  }
	  if ($numpages == '') {
	    global $wp_query;
	    $numpages = $wp_query->max_num_pages;
	    if(!$numpages) {
	        $numpages = 1;
	    }
	  }


	$pagination_args = array(
	    'base'            => get_pagenum_link(1) . '%_%',
	    'format'          => 'page/%#%',
	    'total'           => $numpages,
	    'current'         => $paged,
	    'show_all'        => False,
	    'end_size'        => 1,
	    'mid_size'        => $pagerange,
	    'prev_next'       => True,
	    'prev_text'       => __('<< Prev '),
	    'next_text'       => __('Next >>'),
	    'type'            => 'plain',
	    'add_args'        => false,
	    'add_fragment'    => ''
	  );

	$paginate_links = paginate_links($pagination_args);

	if ($paginate_links) {
	    echo "<nav class='custom-pagination'>";
	      "<span class='page-numbers page-num'>Page " . $paged . " of " . $numpages . "</span> ";
	      echo $paginate_links;
	    echo "</nav>";
	  }

}

/*Svg file uploads*/
function svg_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  $mimes['csv'] = 'text/csv';
  return $mimes;
}
add_filter('upload_mimes', 'svg_mime_types');

add_filter( 'mime_types', 'wpse_mime_types' );
function wpse_mime_types( $existing_mimes ) {
    $existing_mimes['csv'] = 'text/csv';
    return $existing_mimes;
}

/*filter plugin update*/
function filter_plugin_updates( $value ) {
unset( $value->response['advanced-custom-fields-pro/acf.php'] );
return $value;
}
add_filter( 'site_transient_update_plugins', 'filter_plugin_updates' );


/*disable guttenburg editor*/

function isk_disable_gutenberg($is_enabled, $post_type) {
	if ($post_type === 'listorders') return false;
	if ($post_type === 'page') return false;
	if ($post_type === 'post') return false;
	return $is_enabled;
}
add_filter('use_block_editor_for_post_type', 'isk_disable_gutenberg', 10, 2);

/*custom code written on 31dec2019*/
/*Register Custom Post Type List Orders*/
function listorders_post_type() {

	$labels = array(
		'name'                  => _x( 'List Orders', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'List Order', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'List Orders', 'text_domain' ),
		'name_admin_bar'        => __( 'List Orders', 'text_domain' ),
		'archives'              => __( 'List Order Archives', 'text_domain' ),
		'attributes'            => __( 'List Order Attributes', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
		'all_items'             => __( 'All List Orders', 'text_domain' ),
		'add_new_item'          => __( 'Add New List Order', 'text_domain' ),
		'add_new'               => __( 'Add New List Order', 'text_domain' ),
		'new_item'              => __( 'New List Order', 'text_domain' ),
		'edit_item'             => __( 'Edit List Order', 'text_domain' ),
		'update_item'           => __( 'Update List Order', 'text_domain' ),
		'view_item'             => __( 'View List Order', 'text_domain' ),
		'view_items'            => __( 'View List Orders', 'text_domain' ),
		'search_items'          => __( 'Search List Order', 'text_domain' ),
		'not_found'             => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Featured Image', 'text_domain' ),
		'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
		'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
		'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
		'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
		'items_list'            => __( 'Items list', 'text_domain' ),
		'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'List Orders', 'text_domain' ),
		'description'           => __( 'List Orders Type Description', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title','editor', 'revisions', 'author', 'excerpt', 'page-attributes', 'thumbnail', 'custom-fields', 'post-formats' ),
		'taxonomies'            => array( 'listorder_category'),
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
		'capabilities' => array(
		    'create_posts' => false,
		),
		'map_meta_cap' => true,
	);
	register_post_type( 'listorders', $args );

}
add_action( 'init', 'listorders_post_type', 0 );

/*End List Orders*/
/*remove edit vide trash linbk form listorders*/
add_filter( 'page_row_actions', 'isk_page_row_actions', 10, 2 );
function isk_page_row_actions( $actions, $post )
{
    if ( 'listorders' == $post->post_type ) {
        unset( $actions['edit'] );
        unset( $actions['view'] );
        unset( $actions['inline hide-if-no-js'] );
    }
    return $actions;
}