<?php
	get_header();

	$token = $_GET['token'];
	$amt = (float)$_GET['amt'];
	$pid= $_GET['pid'];
	/*echo get_field('payment_mode','options');
	print_r(get_field('test_details','options'));
	print_r(get_field('live_details','options'));
	*/

	$type = "";
	$message = "";
	$apipostarr = array();
		
	if (!empty($_POST['pay_now'])) {
		//print_r($_POST);
		 //die;
		 $apipostarr['card-number']=str_replace(' ','',$_POST['card-number']);
		 /*$apipostarr['card-number']='4242424242424242';*/
		 $apipostarr['month']=$_POST['month2'];
		 $apipostarr['year']=$_POST['year2'];
		 $apipostarr['amount']=$_POST['amount'];
		/*print_r($apipostarr);
		die;*/
	    require_once 'AuthorizeNetPayment.php';
	    $authorizeNetPayment = new AuthorizeNetPayment();
	    $response = $authorizeNetPayment->chargeCreditCard($apipostarr);
	    	  /*echo '<pre>';print_r($response);*/
	         
	    if ($response != null)
	    {
	        $tresponse = $response->getTransactionResponse();

	        /*echo '<pre>';print_r($tresponse);*/
	        
	        if (($tresponse != null) && ($tresponse->getResponseCode()=="1"))
	        {
	            $authCode = $tresponse->getAuthCode();
	            $paymentResponse = $tresponse->getMessages()[0]->getDescription();
	            $reponseType = "success";
	            $message = "This transaction has been approved. <br/> Charge Credit Card AUTH CODE : " . $tresponse->getAuthCode() .  " <br/>Charge Credit Card TRANS ID  : " . $tresponse->getTransId() . "\n";
	        }else{
	            $authCode = "";
	            $paymentResponse = $tresponse->getErrors()[0]->getErrorText();
	            $reponseType = "error";
	            $message = "Charge Credit Card ERROR :  Invalid response\n";
	        }
	        
	        $transactionId = $tresponse->getTransId();
	        $responseCode = $tresponse->getResponseCode();
	        $paymentStatus = $authorizeNetPayment->responseText[$tresponse->getResponseCode()];
	        /*echo 'payment done';*/
	        $param_value_array = array(
	            $transactionId,
	            $authCode,
	            $responseCode,
	            $_POST["amount"],
	            $paymentStatus,
	            $paymentResponse
	        );
	        /*print_r(json_encode($param_value_array));*/
	        update_post_meta($pid,'payment_response',json_encode($param_value_array));
	        update_post_meta($pid,'payment_status',$paymentStatus);
	        update_post_meta($pid,'transactionid',$transactionId);

	        $message = "Thank you for the Payment. It has been completed now admin will approve and will get back to you soon.";

	        /*send mail to admin on success*/
	        $to_email = get_field('admin_emailid','options');
			$subject = 'Payment for Phone and Email Append on DataZapp';
			$headers = array('From: noreply@listability.com','Content-Type: text/html; charset=UTF-8');
			$mailbody = get_field('mail_to_admin','options');
			if(!empty($mailbody)){
				$mailbody = str_replace("{token}",$token,$mailbody);
				$mailbody = str_replace("{name}",$_POST['owner'],$mailbody);
				$mailbody = str_replace("{emailaddress}",get_field('file_owner_email',$pid),$mailbody);
				$mailbody = str_replace("{address}",get_field('address',$pid),$mailbody);
				$mailbody = str_replace("{transid}",$transactionId,$mailbody);				
			}			
			
			$res = wp_mail($to_email,$subject,$mailbody,$headers);
			if($res){
				$uri = site_url();
				header('Refresh: 5; URL='.$uri);
			}else{
				$mnotsent = 'Something Went wrong while seding the mail.';
			}
	        
	    }else{
	        $reponseType = "error";
	        $message= "Charge Credit Card Null response returned";
	    }
	}

?>
 	
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri();?>/apiassets/css/styles.css">
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri();?>/apiassets/css/demo.css">
    <div class="container-fluid">
        
        <div class="creditCardForm">
        	<?php if(!empty($message)) { ?>
		        <div id="response-message" class="<?php echo $reponseType; ?>" style="Color:green;"><?php echo $message; ?></div>
		    <?php  } ?>
		    <?php if(!empty($mnotsent)) { ?>
		        <div id="err-response-message"  style="Color:red;"><?php echo $mnotsent; ?></div>
		    <?php  } ?>
		    
            <div class="heading">
                <h1>Listability Payment</h1>
            </div>
            <div class="payment">
                <form action="" method="post" id="payfrm">
                    <div class="form-group owner">
                        <label for="owner">Owner</label>
                        <input type="text" class="form-control" id="owner" name="owner">
                    </div>
                    <div class="form-group CVV">
                        <label for="cvv">CVV</label>
                        <input type="text" class="form-control" id="cvv" name="cvv">
                    </div>
                    <div class="form-group" id="card-number-field">
                        <label for="cardNumber">Card Number</label>
                        <input type="text" class="form-control" id="cardNumber" name="card-number">
                    </div>
                    <div class="form-group" id="expiration-date">
                        <label>Expiration Date</label>
                        <select name="month2">
                            <option value="01">January</option>
                            <option value="02">February </option>
                            <option value="03">March</option>
                            <option value="04">April</option>
                            <option value="05">May</option>
                            <option value="06">June</option>
                            <option value="07">July</option>
                            <option value="08">August</option>
                            <option value="09">September</option>
                            <option value="10">October</option>
                            <option value="11">November</option>
                            <option value="12">December</option>
                        </select>
                        <select name="year2">
                            <option value="2019"> 2019</option>
                            <option value="2020"> 2020</option>
                            <option value="2021"> 2021</option>
                            <option value="2022"> 2022</option>
                            <option value="2023"> 2023</option>
                            <option value="2024"> 2024</option>
                            <option value="2025"> 2025</option>
                            <option value="2026"> 2026</option>
                            <option value="2027"> 2027</option>
                            <option value="2028"> 2028</option>
                            <option value="2029"> 2029</option>
                            <option value="2030"> 2030</option>
                        </select>
                    </div>
                    <div class="form-group" id="credit_cards">
                        <img src="<?php echo get_stylesheet_directory_uri();?>/apiassets/images/visa.jpg" id="visa">
                        <img src="<?php echo get_stylesheet_directory_uri();?>/apiassets/images/mastercard.jpg" id="mastercard">
                        <img src="<?php echo get_stylesheet_directory_uri();?>/apiassets/images/amex.jpg" id="amex">
                    </div>
                    <div class="form-group" id="pay-now">
                    	<input type='hidden' name='amount' value='<?php echo $amt;?>'> 
                    	<input type="hidden" name="pay_now" value="submit">
                        <button type="submit" class="btn btn-default" id="confirm-purchase">Confirm</button>
                    </div>
                </form>
            </div>
        </div>


        <?php if(get_field('payment_mode','options')=='test'){ ?>
        	<p class="examples-note">Here are some dummy credit card numbers and CVV codes so you can test out the form:</p>
	        <div class="examples">
	            <div class="table-responsive">
	                <table class="table table-hover">
	                    <thead>
	                        <tr>
	                            <th>Type</th>
	                            <th>Card Number</th>
	                            <th>Security Code</th>
	                        </tr>
	                    </thead>
	                    <tbody>
	                        <tr>
	                            <td>Visa</td>
	                            <td>4716108999716531</td>
	                            <td>257</td>
	                        </tr>
	                        <tr>
	                            <td>Master Card</td>
	                            <td>5281037048916168</td>
	                            <td>043</td>
	                        </tr>
	                        <tr>
	                            <td>American Express</td>
	                            <td>342498818630298</td>
	                            <td>3156</td>
	                        </tr>
	                    </tbody>
	                </table>
	            </div>
	        </div>
        <?php } ?>
        

    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="<?php echo get_stylesheet_directory_uri();?>/apiassets/js/jquery.payform.min.js" charset="utf-8"></script>
    <script src="<?php echo get_stylesheet_directory_uri();?>/apiassets/js/script.js"></script>


<?php get_footer(); ?>